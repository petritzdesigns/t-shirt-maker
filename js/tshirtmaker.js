var color = "#d1d1d1";
var textcolor = "#0085ff";
var textcolor2 = "#1768b3";
var shirtcolor = '#ffffff';
var text = "Test";
var textleft = 14;
var text2 = "Shirt";
var textleft2 = 14;

$(document).ready(function () {

    reload();

    $("#color").colorpicker();

    $('#color').colorpicker().on('changeColor', function (e) {
        console.log("Changed!");
        color = $(this).val();
        reload();
    });

    $("#color").keydown(function () {
        console.log("Keypressed!");
        color = $(this).val();
        reload();
    });

    $("#color").change(function () {
        console.log("Changed!");
        color = $(this).val();
        reload();
    });

    $("#textcolor").colorpicker();

    $('#textcolor').colorpicker().on('changeColor', function (e) {
        console.log("Changed!");
        textcolor = $(this).val();
        reload();
    });

    $("#textcolor").keydown(function () {
        console.log("Keypressed!");
        textcolor = $(this).val();
        reload();
    });

    $("#textcolor").change(function () {
        console.log("Changed!");
        textcolor = $(this).val();
        reload();
    });

    $("#textcolor2").colorpicker();

    $('#textcolor2').colorpicker().on('changeColor', function (e) {
        console.log("Changed!");
        textcolor2 = $(this).val();
        reload();
    });

    $("#textcolor2").keydown(function () {
        console.log("Keypressed!");
        textcolor2 = $(this).val();
        reload();
    });

    $("#textcolor2").change(function () {
        console.log("Changed!");
        textcolor2 = $(this).val();
        reload();
    });

    $("#shirtcolor").colorpicker();

    $('#shirtcolor').colorpicker().on('changeColor', function (e) {
        console.log("Changed!");
        shirtcolor = $(this).val();
        reload();
    });

    $("#shirtcolor").keydown(function () {
        console.log("Keypressed!");
        shirtcolor = $(this).val();
        reload();
    });

    $("#shirtcolor").change(function () {
        console.log("Changed!");
        shirtcolor = $(this).val();
        reload();
    });

    $("#bold").click(function () {
        console.log("Clicked");
        reload();
    });

    $("#italic").click(function () {
        console.log("Clicked");
        reload();
    });

    $("#bigger").click(function () {
        console.log("Clicked");
        reload();
    });

    $("#bold2").click(function () {
        console.log("Clicked");
        reload();
    });

    $("#italic2").click(function () {
        console.log("Clicked");
        reload();
    });

    $("#bigger2").click(function () {
        console.log("Clicked");
        reload();
    });
});

function saveShirt() {
    var c = document.getElementById("drawer");
    var image = c.toDataURL("image/png");
    window.open(
            image,
            '_blank'
            );

    console.log("Save!");
}

function reload() {

    color = $("#color").val();

    var c = document.getElementById("drawer");
    var ctx = c.getContext("2d");
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.fillStyle = color;

    ctx.fillRect(0, 0, c.width, c.height);

    var svg = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="1000px" height="1000px" viewBox="0 0 1000 1000" style="enable-background:new 0 0 1000 1000;" xml:space="preserve"> <path style="fill-rule:evenodd;clip-rule:evenodd;fill:' + shirtcolor + ';stroke:#000000;stroke-width:3;" d="M252.18,74.228l-11.242,8.992 L175.29,97.609l-88.581,55.307c0,0,6.295,60.703,34.173,73.294l56.655-15.738c0,0,12.14,116.008,8.094,293.619 c0,0,30.022,7.043,129.948,2.699c82.737-3.598,119.607,3.148,119.607,3.148s-6.745-274.285-6.745-296.769l50.811,11.242 c0,0,25.18-15.738,34.174-75.541L414.952,98.06l-62.95-14.389l-7.194-8.094c0,0-50.361,18.886-92.18-1.798L252.18,74.228z"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;stroke-width:3;" d="M512.076,149.769 c0,0-31.025,31.025-34.174,75.541"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;stroke-width:3;" d="M86.709,152.916 c0,0,34.173,31.027,34.173,73.294"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;" d="M97.051,147.521c0,0,31.926,22.483,35.972,75.541"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;" d="M176.639,96.262c0,0,20.684,97.123,0,115.559"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;" d="M252.18,74.228c0,0,0,29.677,44.964,29.227 c44.966-0.451,47.664-27.878,47.664-27.878"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;" d="M259.822,91.315c0,0,42.268,13.039,76.44,0"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;" d="M240.938,83.22c0,0,13.939,32.375,56.206,32.825 c42.268,0.451,56.208-31.926,56.208-31.926"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;" d="M428.441,213.169c0,0-22.031-4.496-14.389-116.459"/><path style="fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#000000;" d="M466.662,221.263c0,0,1.35-39.569,37.32-77.79"/></svg>';
    ctx.drawSvg(svg, -64, -55, 900, 900);

    var font = "";
    var characters = 14;
    if ($("#bold").prop('checked')) {
        font += "bold ";
    }
    if ($("#italic").prop('checked')) {
        font += "italic ";
    }
    if ($("#bigger").prop('checked')) {
        font += "30px ";
        characters = 10;
    } else {
        font += "20px ";
    }

    font += "Sans-Serif";

    ctx.fillStyle = textcolor;
    ctx.font = font;
    text = $("#text").val();
    text = text.substr(0, characters);
    textleft = characters - text.length;
    $("#textleft").text(textleft + " verbleibend...");
    $("#text").val(text);
    var textwidth = (c.width / 2) - (ctx.measureText(text).width / 2);
    var textheight = (c.height / 2) - 20;
    ctx.fillText(text, textwidth, textheight);

    var font2 = "";
    var characters2 = 14;
    if ($("#bold2").prop('checked')) {
        font2 += "bold ";
    }
    if ($("#italic2").prop('checked')) {
        font2 += "italic ";
    }
    if ($("#bigger2").prop('checked')) {
        font2 += "30px ";
        characters2 = 10;
    } else {
        font2 += "20px ";
    }

    font2 += "Sans-Serif";

    ctx.fillStyle = textcolor2;
    ctx.font = font2;
    text2 = $("#text2").val();
    text2 = text2.substr(0, characters2);
    textleft2 = characters2 - text2.length;
    $("#textleft2").text(textleft2 + " verbleibend...");
    $("#text2").val(text2);
    var textwidth = (c.width / 2) - (ctx.measureText(text2).width / 2);
    var textheight = (c.height / 2) + 10;
    ctx.fillText(text2, textwidth, textheight);

    console.log("Reload!");
}

function send() {
    
    bgColor = color;
    textz1 = $("#text").val();
    bold = $("#bold").prop('checked');
    italic = $("#italic").prop('checked');
    bigger = $("#bigger").prop('checked');
    textz2 = $("#text2").val();
    bold2 = $("#bold2").prop('checked');
    italic2 = $("#italic2").prop('checked');
    bigger2 = $("#bigger2").prop('checked');
    textf1 = textcolor;
    textf2 = textcolor2;
    tshirtfarbe = shirtcolor;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function ()
    {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200)
        {
            console.log(xmlhttp.responseText);
        }
    };
    xmlhttp.open("GET", "send.php?bgcolor=" + bgColor
            + "&textz1=" + textz1
            + "&bold=" + bold
            + "&italic=" + italic
            + "&bigger=" + bigger
            + "&textz2=" + textz2
            + "&bold2=" + bold2
            + "&italic2=" + italic2
            + "&bigger2=" + bigger2
            + "&textf1=" + textf1
            + "&textf2=" + textf2
            + "&tshirtfarbe=" + tshirtfarbe, true);
    xmlhttp.send();

}